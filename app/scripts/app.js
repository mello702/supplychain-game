'use strict';

/**
 * @ngdoc overview
 * @name dwrepoApp
 * @description
 * # dwrepoApp
 *
 * Main module of the application.
 */
angular
  .module('dwrepoApp', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngNumeraljs',
    'ngMaterial'
  ])

  .config(function ($routeProvider) {
      

      $routeProvider
        .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
        })
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
  });
