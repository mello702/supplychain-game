'use strict';
/*global _:true*/
/*global Clay:true*/

/**
 * @ngdoc function
 * @name dwrepoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dwrepoApp
 */
angular.module('dwrepoApp')
  .controller('MainCtrl', function ($scope, $timeout, $filter, GameEngine, $mdDialog) {

      // Store state
      $scope.newGame = { cash: 500, days: '7' };
      $scope.debug = false;
      $scope.state = GameEngine.getState();
      $scope.travelling = false;
      $scope.qtyBuy = 0;
      $scope.qtySell = 0;
      $scope.highscore = { name: '', msg: '', score: 0, log: [] };
      $scope.trans = { qtyBuy: 0, qtySell: 0 };
      $scope.view = 'newgame'; // market, travel
      $scope.leaderboard = [];

      $scope.loadscores = function () {
          var leaderboard = new Clay.Leaderboard({ id: 5020 });
          leaderboard.fetch({}, function (res) {
              $scope.leaderboard = res;
      
          });
      };

      $scope.showSubmitScore = true;
      $scope.submitscore = function () {
          //          var leaderboardId = 'CgkIpPqp0toXEAIQAA';
          //        var url = 'https://www.googleapis.com/games/v1/leaderboards/' + leaderboardId + '/scores?score=' + $scope.state.player.money;
          //5020


          var leaderboard = new Clay.Leaderboard({ id: 5020 });
          leaderboard.post({ name: $scope.highscore.name, hideUI: true, score: $scope.state.player.money }, function () {
              $timeout(function () { $scope.showSubmitScore = false; });

              $scope.loadscores();
          });

      };

      // market, travel
      $scope.market = []; //GameEngine.getMarket();
      $scope.cities = []; //GameEngine.getTravel();
      $scope.currentItem = {};

      $scope.timeLeft = function () {
          var seconds = $scope.state.player.hoursRemaining * 60 * 60;

          //var numyears = Math.floor(seconds / 31536000);
          var numdays = Math.floor((seconds % 31536000) / 86400);
          var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
          //var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
          //var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;

          var result = '';
          if (numhours > 0) {
              result = numhours + 'hrs';
          }

          if (numdays > 0) {
              result = numdays + ' days ' + result;
          }

          return result;

      };

      $scope.getPercentDone = function () {
          var maxHours = ($scope.state.player.maxDays * 24) + 0.01;
          var usedHours = maxHours - $scope.state.player.hoursRemaining;
          return (usedHours / maxHours) * 100;
      };

      $scope.endGameCheck = function (showDialog) {
          // Check if it's the last day
          var hasItems = false;
          _.each($scope.state.player.inventory, function (item) {
              if (item.qty > 0) { hasItems = true; }
          });


          if ($scope.state.lastDay) {

              if (hasItems) {

                  if (showDialog) {
                      $mdDialog.show(
             $mdDialog.alert()
               .title('Your time is up!')
               .content('Today is the last day to conduct business before the game ends. You must sell all your inventory to end the game!')
               .ariaLabel('Last day')
               .ok('Got it!')
                      //.targetEvent(ev)
           );
                  }
              } else {
                  // Has no items, end game

                  $scope.finish();

              }
          }
      };

      $scope.init = function (delay) {

          $scope.endGameCheck(true);

          $timeout(function () {
              $scope.market = GameEngine.getMarket();
              $scope.cities = GameEngine.getTravel();

              $scope.loadscores();

          }, delay);
      };



      // market operations
      $scope.myInventory = function (item) {
          var inv = 0;

          var invItem = $scope.state.player.inventory[item.id];
          if (invItem) {
              inv = invItem.qty;
          }

          return inv;
      };

      $scope.myInventoryAvgCost = function (item) {

          var amount = 0;
          var invItem = $scope.state.player.inventory[item.id];
          if (invItem) {
              amount = invItem.avgCost;
          }

          return amount;
      };

      $scope.maxCanBuy = function (item) {
          var amount = Math.floor($scope.state.player.money / item.marketPrice);
          if (amount < 0) { amount = 0; }

          return amount;
      };

      $scope.canBuy = function (item) {
          return $scope.state.player.money >= item.marketPrice;
      };

      $scope.toggleShowItem = function (item) {
          //$scope.qtyBuy = 0;
          //$scope.qtySell = 0;
          $scope.trans.qtyBuy = 0;
          $scope.trans.qtySell = 0;

          if ($scope.currentItem.id === item.id) {
              // Hide panel
              $scope.currentItem = {};
              //console.log('hide');
          } else {
              // Show panel              
              $scope.trans.qtyBuy = $scope.maxCanBuy(item);
              $scope.trans.qtySell = $scope.myInventory(item);
              $scope.currentItem = item;
          }
      };

      $scope.buy = function (item) {
          var amount = parseInt($scope.trans.qtyBuy);

          if (amount < 1) { return; }
          GameEngine.buyItem(item, amount);
          $scope.currentItem = {};
          $scope.trans.qtyBuy = 0;
      };

      $scope.buyAll = function (item) {
          var amount = $scope.maxCanBuy(item);
          GameEngine.buyItem(item, amount);
          $scope.currentItem = {};
      };

      $scope.isCheap = function (item) {
          return item.marketPrice <= (item.avgPrice * 0.9);
      };

      $scope.canSell = function (item) {
          return $scope.myInventory(item);
      };

      $scope.sell = function (item) {
          var amount = parseInt($scope.trans.qtySell);
          if (amount < 1) { return; }

          GameEngine.sellItem(item, amount);
          $scope.currentItem = {};
          $scope.trans.qtySell = 0;
          $scope.endGameCheck(false);
      };

      $scope.sellAll = function (item) {
          var amount = $scope.myInventory(item);
          GameEngine.sellItem(item, amount);
          $scope.currentItem = {};

          $scope.endGameCheck(false);
      };


      // travel operations
      $scope.canTravel = function (city) {
          return $scope.state.player.money >= city.ticketPrice;
      };

      $scope.currentCity = function (city) {
          return $scope.state.player.location.id === city.id;
      };

      $scope.sleep = function (city) {
          // Sleep the night in current city          
          $scope.market = [];
          $scope.cities = [];
          $scope.currentItem = {};
          $scope.state.player.money -= city.ticketPrice;

          GameEngine.sleep();

          $scope.view = 'market';

          $scope.init(1);



      };

      $scope.travelTo = function (city) {
          $scope.market = [];
          $scope.cities = [];
          $scope.currentItem = {};

          GameEngine.travelTo(city);
          $scope.init(1);
      };

      $scope.finish = function () {
          // finish game off by advancing to the next day.
          $scope.changeView('gameover');
          GameEngine.log('Game over! Earned ' + $filter('currency')($scope.state.player.money, '$', 0) + ' over ' + $scope.state.player.maxDays + ' days', 'game');

          $scope.state.started = false;
      };

      // Nav
      $scope.tabSelected = function (view) {
          if ($scope.state.started) {
              $scope.changeView(view);
          }
      };

      $scope.changeView = function (view) {
          //console.log('old view ' + $scope.view + ' new view ' + view);

          $scope.view = view;
      };

      $scope.restart = function () {
          //$('.container').hide();
          $scope.changeView('market');
          $scope.showSubmitScore = true;

          $timeout(function () {

              var startCity = GameEngine.restartGame(parseInt($scope.newGame.days), $scope.newGame.cash);

              $scope.state = GameEngine.getState();

              $scope.travelTo(startCity);
              //$scope.init(50);
          }, 10);
      };


  });
