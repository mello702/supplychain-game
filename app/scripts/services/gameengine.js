﻿/// <reference path="../../../Gruntfile.js" />
/*global _:true*/


'use strict';

angular.module('dwrepoApp')
    .factory('GameEngine', function ($filter) {
        var service = {};

        // Game configs
        service.config = {
            startingCash: 500,
            maxDays: 2,
            allowDebt: true,

            travelMinCostPerKm: 0.2,
            travelMaxCostPerKm: 0.8,

            locationMinMultiplier: 0.3,
            locationMaxMultiplier: 1.7,

            itemMinProfitPercent: 37,
            itemMaxProfitPercent: 175,

            sleepTimeMs: 10,
            travelTimeMs: 150,

            hotelMinCost: 12,
            hotelMaxCost: 75,

            sleepMinHours: 2,
            sleepMaxHours: 16,

            flightMinSpeed: 400,
            flightMaxSpeed: 600,

            saleMinTime: 0.0001,
            saleMaxTime: 0.001,
        };

        // Setup cities
        var locations = [];
        locations.push({
            id: 'vegas', name: 'Las Vegas', multiplier: 1, ticketPrice: 0,
            lat: 36.1215, long: 115.1739, dist: 0
        });
        locations.push({
            id: 'la', name: 'Los Angeles', multiplier: 1, ticketPrice: 0,
            lat: 34.0500, long: 118.2500, dist: 0
        });
        locations.push({
            id: 'nyc', name: 'New York', multiplier: 1, ticketPrice: 0,
            lat: 40.7127, long: 74.0059, dist: 0
        });
        locations.push({
            id: 'kc', name: 'Kansas City', multiplier: 1, ticketPrice: 0,
            lat: 39.1067, long: 94.6764, dist: 0
        });

        //locations.push({
        //    id: 'mum', name: 'Mumbai', multiplier: 1, ticketPrice: 0,
        //    lat: 18.9750, long: 72.8258, dist: 0
        //});
        //locations.push({
        //    id: 'hong', name: 'Hong Kong', multiplier: 1, ticketPrice: 0,
        //    lat: 22.2670, long: 114.1880, dist: 0
        //});
        //locations.push({
        //    id: 'lond', name: 'London', multiplier: 1, ticketPrice: 0,
        //    lat: 51.5072, long: 0.1275, dist: 0
        //});

        // Setup items

        var items = [];
        items.push({
            id: 'paperclip', name: 'Paper Clips', marketPrice: 0, avgPrice: 0,
            minPrice: 2, maxPrice: 50
        });
        items.push({
            id: 'phone', name: 'Desk Phones', marketPrice: 0, avgPrice: 0,
            minPrice: 15, maxPrice: 100
        });
        items.push({
            id: 'filecab', name: 'File Cabinets', marketPrice: 0, avgPrice: 0,
            minPrice: 35, maxPrice: 200
        });
        items.push({
            id: 'printer', name: 'Printers', marketPrice: 0, avgPrice: 0,
            minPrice: 50, maxPrice: 400
        });
        items.push({
            id: 'laptop', name: 'Laptops', marketPrice: 0, avgPrice: 0,
            minPrice: 200, maxPrice: 1000
        });
        items.push({
            id: 'server', name: 'Servers', marketPrice: 0, avgPrice: 0,
            minPrice: 400, maxPrice: 5000
        });
        items.push({
            id: 'contract', name: 'Contracts', marketPrice: 0, avgPrice: 0,
            minPrice: 1300, maxPrice: 25000
        });

        
        items.push({
            id: 'kiosk', name: 'Mall Kiosks', marketPrice: 0, avgPrice: 0,
            minPrice: 8000, maxPrice: 45000
        });

        items.push({
            id: 'storefront', name: 'Store Fronts', marketPrice: 0, avgPrice: 0,
            minPrice: 31000, maxPrice: 310000
        });

        items.push({
            id: 'stripmall', name: 'Strip Malls', marketPrice: 0, avgPrice: 0,
            minPrice: 150000, maxPrice: 1260000
        });

        items.push({
            id: 'bizpark', name: 'Business Parks', marketPrice: 0, avgPrice: 0,
            minPrice: 510000, maxPrice: 15260000
        });

        items.push({
            id: 'tower', name: 'Office Towers', marketPrice: 0, avgPrice: 0,
            minPrice: 3530000, maxPrice: 43260000
        });

        
        // State
        var defaultState = {
            started: false,
            gameOver: false,
            lastDay: false,
            player: {
                money: service.config.startingCash,
                location: {}, //locations[0],
                inventory: {},
                days: 0,
                maxDays: service.config.maxDays,
                log: [],

                hoursRemaining: 0,
            },
        };


        // Setup state
        service.state = _.cloneDeep(defaultState);
        service.getState = function () { return service.state; };
        service.restartGame = function (maxDays, startingCash) {
            service.state = _.cloneDeep(defaultState);

            if (maxDays) {
                service.state.player.maxDays = maxDays;
            }

            service.state.player.hoursRemaining = service.state.player.maxDays * 24;

            if (startingCash) {
                service.state.player.money = startingCash;
            }

            // Start in a random city
            var locationIndex = _.random(0, locations.length - 1);
            locations[locationIndex].ticketPrice = 0;

            // Game has started
            service.state.started = true;

            service.log('Started new game (' + service.state.player.maxDays + ' days, ' + $filter('currency')(service.state.player.money, '$', 0) + ' cash)', 'game');
            // Return city so player can travel there
            return locations[locationIndex];
        };

        // get Market
        service.getMarket = function () {
            // Set item prices
            var market = _.each(items, function (item) {
                var margin = (_.random(service.config.itemMinProfitPercent, service.config.itemMaxProfitPercent)) / 100;
                var basePrice = _.random(item.minPrice, item.maxPrice);

                item.avgPrice = ((item.minPrice, item.maxPrice) / 2) * service.state.player.location.multiplier;
                item.marketPrice = basePrice * margin * service.state.player.location.multiplier;
                item.marketPrice = Math.round(item.marketPrice);
            });

            return market;
        };

        service.buyItem = function (item, qty) {
            var subtotal = item.marketPrice * qty;
            var hasEnoughMoney = service.state.player.money >= subtotal;

            if (hasEnoughMoney || service.config.allowDebt) {
                service.state.player.money -= subtotal;


                if (!service.state.player.inventory.hasOwnProperty(item.id)) {
                    service.state.player.inventory[item.id] = { qty: 0, cost: 0, avgCost: 0 };
                }
                service.state.player.inventory[item.id].qty += qty;
                service.state.player.inventory[item.id].cost += subtotal;
                service.state.player.inventory[item.id].avgCost = Math.round(service.state.player.inventory[item.id].cost / service.state.player.inventory[item.id].qty);



                //var timePerItem = _.random(service.config.saleMinTime, service.config.saleMaxTime);
                //var time = timePerItem * qty;
                //service.advanceTime(time);
                //console.log('sale took ' + time);

                service.log('Bought ' + qty + ' ' + item.name + ' at ' + $filter('currency')(item.marketPrice, '$', 0) + '/ea (' + $filter('currency')(subtotal, '$', 0) + ')', 'market');

                //console.log('bought ' + qty + ' of ' + item.name + ' total ' + subtotal);
            } else {
                console.log('too much ' + subtotal + ' > ' + service.state.player.money);
            }
        };

        service.sellItem = function (item, qty) {
            var subtotal = item.marketPrice * qty;
            var canSell = true;
            if (!service.state.player.inventory.hasOwnProperty(item.id)) {
                service.state.player.inventory[item.id] = { qty: 0, cost: 0, avgCost: 0 };
            }

            canSell = service.state.player.inventory[item.id].qty >= qty;

            if (canSell) {
                service.state.player.money += subtotal;
                service.state.player.inventory[item.id].qty -= qty;
                if (service.state.player.inventory[item.id].qty === 0) {
                    service.state.player.inventory[item.id].avgCost = 0;
                }

                //var timePerItem = _.random(service.config.saleMinTime, service.config.saleMaxTime);
                //var time = timePerItem * qty;
                //service.advanceTime(time);
                //console.log('sale took ' + time);

                service.log('Sold ' + qty + ' ' + item.name + ' at ' + $filter('currency')(item.marketPrice, '$', 0) + '/ea (' + $filter('currency')(subtotal, '$', 0) + ')', 'market');

                //console.log('sold ' + qty + ' of ' + item.name + ' total ' + subtotal);
            } else {
                console.log('you do not have ' + qty + ' ' + item.name);
            }
        };


        // travel
        service.getTravel = function () {
            var travelPricePerKm = _.random(service.config.travelMinCostPerKm, service.config.travelMaxCostPerKm);
            var cities = _.each(locations, function (city) {

                var current = service.state.player.location;
                var dest = city;
                var dist = calcCrow(current.lat, current.long, dest.lat, dest.long);

                //console.log('dist from ' + current.name + ' to ' + dest.name + ' ' + dist);

                if (dest === current) {
                    // Hotel price
                    city.ticketPrice = _.random(service.config.hotelMinCost, service.config.hotelMaxCost);
                } else {
                    // Airline price
                    city.ticketPrice = travelPricePerKm * dist;
                }

                city.ticketPrice = Math.round(city.ticketPrice);
                city.dist = Math.floor(dist);
            });

            return _.sortBy(cities, function (item) { return item.ticketPrice; });
        };

        service.sleep = function () {
            var time = _.random(service.config.sleepMinHours, service.config.sleepMaxHours);
            var msg = 'Slept in hotel room for ' + $filter('number')(time) + 'hrs';
            //console.log(msg);
            service.log(msg, 'time');
            service.advanceTime(time);
        };

        service.travelTo = function (city) {

            var hasEnoughMoney = service.state.player.money >= city.ticketPrice;
            if (hasEnoughMoney || service.config.allowDebt) {
                service.state.player.money -= city.ticketPrice;

                var speed = _.random(service.config.flightMinSpeed, service.config.flightMaxSpeed);
                var flyHours = city.dist / speed; //km/h plane speed
                service.advanceTime(flyHours);

                service.state.player.location = city;

                service.state.player.location.multiplier = _.random(service.config.locationMinMultiplier, service.config.locationMaxMultiplier);
                //console.log('travel to ' + city.name);

                var text = '';

                if (city.dist > 0) {
                    text += 'Flew ' + city.dist + 'km';
                } else {
                    text += 'Traveled';
                }

                text += ' to ' + city.name;

                if (city.ticketPrice > 0) {
                    text += ' in ' + $filter('number')(flyHours) + 'hrs for ' + $filter('currency')(city.ticketPrice, '$', 0);
                }

                service.log(text, 'travel');
            } else {
                // Cannot afford ticket
                console.log('cannot afford ticket ' + city.ticketPrice + ' > ' + service.state.player.money);
            }
        };

        service.advanceTime = function (hours) {
            //service.state.player.days++;
            service.state.player.hoursRemaining -= hours;
            service.state.lastDay = service.state.player.hoursRemaining <= 24;

            if (service.state.player.hoursRemaining <= 0) {
                service.state.gameOver = true;
            }
        };

        // Loggin
        service.log = function (action, type) {
            var entry = {
                type: type || '',
                date: new Date().getTime(),
                day: 'Day ' + service.state.player.days,
                text: action,
                location: _.clone(service.state.player.location)
            };

            service.state.player.log.unshift(entry);

            ////try {
            //    var o = $mdToast.simple()
            //        .content(action)
            //        .hideDelay(0);

            //    o = $mdToast.build();
            //    var h = $mdToast.show(o);
            //    //$mdToast.hide(h);
            ////} catch (e) { }



        };

        //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
        function calcCrow(lat1, lon1, lat2, lon2) {
            var R = 6371; // km
            var dLat = toRad(lat2 - lat1);
            var dLon = toRad(lon2 - lon1);
            var lat1rad = toRad(lat1);
            var lat2rad = toRad(lat2);

            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
              Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1rad) * Math.cos(lat2rad);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d;
        }

        // Converts numeric degrees to radians
        function toRad(Value) {
            return Value * Math.PI / 180;
        }


        return service;
    });
